import { Authenticator } from "./Authenticator";

import * as rules from './rules';

const checker = new Authenticator();

checker
  .add(rules.checkName)
  .add(rules.checkPass)
  .add(rules.checkAuth);

export const authChecker = authData => {
  return checker.check(authData);
};