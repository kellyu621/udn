/** 實際上真實的資料應該會放在後端而不是這裡 */
import dbData from "data/account";

/**  帳號 5 位以上英數，首字英文 */
export const checkName = ({ name }) => {
  if(!/^[A-Za-z]\w{4,}/g.test(name)) return 'invalidName';
  return null;
};

/** 密碼 6 - 20英數混合 開眼 */
export const checkPass = ({ password }) => {
  if(!/\w{6,20}/g.test(password)) return 'invalidPassWord';
  return null;
};

export const checkAuth = (authData) => {
  const dbAccount = dbData.find(({ name }) => name === authData.name);
  if(!dbAccount) return 'invalidAuth';
  if(authData.password !== dbAccount.password) return 'invalidAuth';
  return null;
};