import {
  Route,
  Redirect,
} from "react-router-dom";

import {
  useAuth,
} from "hooks";

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
export function PrivateRoute({ children, ...rest }) {
  const auth = useAuth();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth.user ?
          children :
          <Redirect
            to={{
              pathname: "/",
              state: { from: location }
            }}
          />
      }
    />
  );
}