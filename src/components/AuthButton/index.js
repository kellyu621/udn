import { useHistory } from "react-router-dom";
import { useAuth } from "hooks";
import style from './style.module.css';

// 登入頁
export function AuthButton() {
  const auth = useAuth();
  const history = useHistory();
  // 導去首頁
  const handleLogout = () => {
    auth.signout(() => history.push("/"));
  };

  return (
    <div>
      {auth.user ? (
        <div className={style.banner}>
          Welcome! {auth.user}
          <button
            className={style.signout}
            onClick={handleLogout}>
          Sign out
          </button>
        </div>
      ) : ""
      }
    </div>
  );
}