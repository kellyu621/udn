import { useState } from "react";
import { authChecker } from "common/AuthChecker";

const fakeAuth = {
  isAuthenticated: false,
  signin(cb) {
    fakeAuth.isAuthenticated = true;
    setTimeout(cb, 100); // fake async
  },

  /**
   * cb is
   * () => {
      setUser(null);
      cb();
    }
   */
  signout(cb) {
    fakeAuth.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};

// 會提供 user 的狀態: 登入 or 未登入
export function useProvideAuth() {
  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);

  /**
    () => {
      history.replace(from);
    }
   */
  const signin = (authRequest, cb) => {
    const cause = authChecker(authRequest);
    if(cause) {
      return setError(cause);
    }
    fakeAuth.signin(() => {
      setError(null);
      setUser("user");
      cb();
    });
  };

  const signout = cb => { //  cb is () => history.push("/")
    fakeAuth.signout(() => {
      setUser(null);
      cb();
    });
  };

  return {
    user,
    error,
    signin,
    signout
  };
}