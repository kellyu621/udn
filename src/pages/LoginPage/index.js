import { useRef } from "react";
import {
  useHistory,
  useLocation
} from "react-router-dom";
import { PATH } from "route";
import { useAuth } from "hooks";
import { Input, Button, Form } from 'antd';
import style from './style.module.css';

export function LoginPage() {
  const auth = useAuth();
  const nameDOM = useRef();
  const passDOM = useRef();
  const history = useHistory();
  const location = useLocation();
  const { from } = location.state || { from: { pathname: PATH.LIST } };
  const login = e => {
    e.preventDefault();
    const authRequest = {
      name: nameDOM.current.input.value,
      password: passDOM.current.input.value,
    };
    auth.signin(authRequest, () => {
      history.replace(from);
    });
  };

  return (
    <div className={style.container}>
      <Form onSubmit={login} className={style.form}>
        <div className={style.text}>聯合智網股份有限公司</div>
        {/* <p>You must log in to view the page at {from.pathname}</p> */}
        <Input
          className={style.input}
          type="text"
          placeholder="username"
          ref={nameDOM}
        />
        <Input.Password
          className={style.input}
          placeholder="password"
          ref={passDOM}
        />
        <Button
          className={style.button}
          onClick={login}
          htmlType="submit"
          block
        >登 入</Button>
        {auth.error &&
          <div className={style.error}>
            帳號或密碼不正確
          </div>
        }
      </Form>
    </div>
  );
}
