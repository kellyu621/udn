import { PATH } from "route";
import { Icon } from "antd";
import { Link } from "react-router-dom";
import style from './style.module.css';

export default function ActionButtons({ id }) {
  // console.warn(PATH.DETAIL_PAGE(id));
  return (
    <>
      <Link
        className={style.link}
        to={PATH.DETAIL_PAGE(id)}>
        <Icon type="eye" />
      </Link>
      <Link
        className={style.link}
        to={PATH.EDIT_PAGE(id)}>
        <Icon type="edit" />
      </Link>
    </>
  );
}
