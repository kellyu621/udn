import { useState, useRef } from "react";
import { data } from "data/product.json";
import { Table, Input, Button, Form } from "antd";
import ActionButtons from "./components/ActionButtons";
import style from './style.module.css';

const columns = [
  {
    title: '品牌名稱',
    dataIndex: 'brand',
    key: 'brand',
  },
  {
    title: '商品名稱',
    dataIndex: 'name',
    key: 'name',
    sorter: (a, b) => a.name > b.name ? 1 : -1,
  },
  {
    title: '商品編號',
    dataIndex: 'id',
    key: 'id',
    sorter: (a, b) => a.id > b.id ? 1 : -1,
  },
  {
    title: '價格',
    dataIndex: 'price',
    key: 'price',
    sorter: (a, b) => a.price > b.price ? 1 : -1,
  },
  {
    title: '規格ㄧ',
    dataIndex: 'color',
    key: 'color',
  },
  {
    title: '規格二',
    dataIndex: 'size',
    key: 'size',
  },
  {
    title: '動作',
    dataIndex: '',
    key: 'x',
    render: (product) => <ActionButtons id={product.id + product.size} />
  },
];

export function ListPage() {
  const idDOM = useRef();
  const nameDOM = useRef();
  const [size, setSize] = useState(20);
  const [idFilter, setIdFilter] = useState(new RegExp(""));
  const [nameFilter, setNameFilter] = useState(new RegExp(""));

  const search = e => {
    e.preventDefault();
    setIdFilter(new RegExp(idDOM.current.input.value));
    setNameFilter(new RegExp(nameDOM.current.input.value));
  };

  const clear = () => {
    idDOM.current.input.value =  "";
    nameDOM.current.input.value =  "";
  };

  const filterData = data
    .filter(({id, name}) => idFilter.test(id) && nameFilter.test(name));

  return (
    <div className={style.container}>
      <Form className={style.form} onSubmit={search}>
        <div className={style.inputContainer}>
          <div className={style.text}>商品編號</div>
          <Input
            className={style.input}
            type="text"
            placeholder="請輸入"
            ref={idDOM}
            pattern="\w*"
          />
        </div>
        <div className={style.inputContainer}>
          <div className={style.text}>商品名稱</div>
          <Input
            className={style.input}
            type="text"
            placeholder="請輸入"
            ref={nameDOM}
          />
        </div>
        <div className={style.button}>
          <Button
            className={style.clearButton}
            type="primary"
            onClick={clear}
          >
          清除
          </Button>
          <Button
            className={style.searchButton}
            type="primary"
            icon="search"
            htmlType="submit"
          >
          搜尋
          </Button>
        </div>
      </Form>
      <Table
        className={style.table}
        rowKey={({id, size}) => `${id}${size}`}
        dataSource={filterData}
        columns={columns}
        pagination={{
          pageSize: size,
          showQuickJumper: true,
          showSizeChanger: true,
          pageSizeOptions: ['20', '40', '100'],
          onShowSizeChange: (current, size) => {
            setSize(size);
          }
        }}
      />;
    </div>
  );
}
