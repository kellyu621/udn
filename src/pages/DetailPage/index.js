import { PATH } from "route";
import { Button } from "antd";
import { data } from "data/product";
import { NotFoundPage } from "pages";
import style from './style.module.css';
import { Link, useParams } from "react-router-dom";


export function DetailPage() {
  const { id } = useParams();

  const product = data.find(({ id: sid, size }) => `${sid}${size}` === id);
  if(!product) {
    return <NotFoundPage />;
  }

  return (
    <div className={style.container}>
      <div className={style.detail}>
        <h2>商品頁</h2>
        <div>id: {product.id}</div>
        <div>brand: {product.brand}</div>
        <div>name: {product.name}</div>
        <div>price: {product.price}</div>
        <div>color: {product.color}</div>
        <div>size: {product.size}</div>
        <div className={style.link}>
          <Link to={PATH.LIST}>
            <Button className={style.button}>返回列表</Button>
          </Link>
          <Link to={PATH.EDIT_PAGE(id)}>
            <Button
              className={`${style.button} ${style.editbutton}`}
              icon="edit"
            >
              編輯
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
}
