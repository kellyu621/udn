import style from './style.module.css';

export function NotFoundPage() {
  return (
    <div className={style.font}>
      404
      Page Not Found!
    </div>
  );
}
