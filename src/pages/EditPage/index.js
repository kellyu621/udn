import { useRef } from "react";
import { Input, Button } from "antd";
import { Link, useParams, useHistory } from "react-router-dom";

import { PATH } from "route";
import { data } from "data/product";
import { NotFoundPage } from "pages";
import style from './style.module.css';


export function EditPage() {
  const { id } = useParams();
  const history = useHistory();

  const form = {
    idDOM: useRef(),
    brandDOM: useRef(),
    nameDOM: useRef(),
    priceDOM: useRef(),
    colorDOM: useRef(),
    sizeDOM: useRef(),
  };

  const product = data.find(({ id: sid, size }) => `${sid}${size}` === id);
  if(!product) {
    return <NotFoundPage />;
  }

  const handleSubmit = e => {
    e.preventDefault();
    const updateData = {
      id: form.idDOM.current.input.value,
      brand: form.brandDOM.current.input.value,
      name: form.nameDOM.current.input.value,
      price: form.priceDOM.current.input.value,
      color: form.colorDOM.current.input.value,
      size: form.sizeDOM.current.input.value,
    };
    console.log(updateData);

    // save data, but frontend can't save
    /**
      fetch(postURL, {
        method: 'POST'
        body: JSON.stringify(updateData),
      })
     */
    history.push(PATH.DETAIL_PAGE(id));
  };

  return (
    <div className={style.container}>
      <form className={style.detail} onSubmit={handleSubmit}>
        <h2>編輯頁</h2>
        <div>id: <Input ref={form.idDOM} defaultValue={product.id} required pattern="(\w|-)*" /></div>
        <div>brand: <Input ref={form.brandDOM} defaultValue={product.brand} required /></div>
        <div>name: <Input ref={form.nameDOM} defaultValue={product.name} required /></div>
        <div>price: <Input ref={form.priceDOM} defaultValue={product.price} required type="number" /></div>
        <div>color: <Input ref={form.colorDOM} defaultValue={product.color} required /></div>
        <div>size: <Input ref={form.sizeDOM} defaultValue={product.size} required /></div>
        <div className={style.link}>
          <Link to={PATH.LIST}>
            <Button className={style.button}>返回列表</Button>
          </Link>
          {/* <Link to={PATH.DETAIL_PAGE(id)}> */}
          <Button
            icon="save"
            htmlType="submit"
            className={`${style.button} ${style.savebutton}`}
          >
              儲存
          </Button>
          {/* </Link> */}
        </div>
      </form>
    </div>

  );
}
