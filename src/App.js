import {
  Route,
  Switch,
  HashRouter as Router,
} from "react-router-dom";

import {
  EditPage,
  ListPage,
  LoginPage,
  DetailPage,
  NotFoundPage,
} from "pages";

import { PATH } from "route";

import { AuthButton } from "components";

import {
  authContext,
  useProvideAuth,
} from "hooks";

import { PrivateRoute } from "PrivateRoute";

import style from './App.module.scss';


/** For more details on
 * `authContext`, `ProvideAuth`, `useAuth` and `useProvideAuth`
 * refer to: https://usehooks.com/useAuth/
 */
function App() {
  const auth = useProvideAuth();
  return (
    <div className={style.App}>
      <authContext.Provider value={auth}>
        <Router>
          {/* <AuthButton /> */}
          <Switch>
            <Route exact path={PATH.LOGIN}><LoginPage /></Route>
            <PrivateRoute path={PATH.LIST}><ListPage /></PrivateRoute>
            <PrivateRoute path={PATH.DETAIL}><DetailPage /></PrivateRoute>
            <PrivateRoute path={PATH.EDIT}><EditPage /></PrivateRoute>
            <Route path="*"><NotFoundPage /></Route>
          </Switch>
        </Router>
      </authContext.Provider>
    </div>
  );
}

export default App;
