const LOGIN = "/";
const LIST = "/list";

const EDIT = "/:id/edit";
const DETAIL = "/:id/detail";

const EDIT_PAGE = id => `/${id}/edit`;
const DETAIL_PAGE = id => `/${id}/detail`;
// https://reactrouter.com/web/example/url-params

export const PATH = {
  LOGIN,
  LIST,
  EDIT,
  DETAIL,
  EDIT_PAGE,
  DETAIL_PAGE,
};